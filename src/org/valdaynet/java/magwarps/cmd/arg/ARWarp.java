package org.valdaynet.java.magwarps.cmd.arg;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.bukkit.command.CommandSender;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.massivecore.CaseInsensitiveComparator;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.arg.ARAbstract;
import com.massivecraft.massivecore.util.Txt;

public class ARWarp extends ARAbstract<UWarp> {
	
	private UWarp warp;
	private static ARWarp ar = new ARWarp();
	public static ARWarp get() { return ar; }

	@Override
	public Collection<String> getTabList(CommandSender arg0, String arg1) {
		Set<String> tree = new TreeSet<String>(CaseInsensitiveComparator.get());
		for (UWarp warp: UWarpColl.get().getAll())
		{
			tree.add(warp.getName());
		}
		return tree;
	}

	@Override
	public UWarp read(String arg0, CommandSender arg1) throws MassiveException {
		String id = Txt.upperCaseFirst(arg0);
		this.warp = UWarpColl.getByName(id);
		return this.warp;
	}
	
}