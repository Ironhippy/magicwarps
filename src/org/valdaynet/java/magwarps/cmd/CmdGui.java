package org.valdaynet.java.magwarps.cmd;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.valdaynet.java.magwarps.MagicWarps;
import org.valdaynet.java.magwarps.OptionalDependencies;
import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.entity.MConf;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.util.Txt;

import net.milkbowl.vault.economy.Economy;

public class CmdGui extends MagicCommand {
	
	public CmdGui()
	{
		this.addAliases("book", "b");
		
		this.desc = "open teleportation gui.";
		
		this.addRequirements(ReqIsPlayer.get(), ReqHasPerm.get(Perm.TELEPORT.node));
	}
	
	@Override
	public void perform() throws MassiveException {
		
		if (OptionalDependencies.get().isVaultEnabled())
		{
			Economy eco = MagicWarps.get().getEconomy();
			if (eco.getBalance(me)>MConf.get().getBookCharge())
			{
			    eco.withdrawPlayer(me, MConf.get().getBookCharge());
			}
			else {
				sendMessage(Txt.parse("<rose>You have insufficient funds. <i>You cannot afford a warp at this time."));
				return;
			}
		}
		
		sendMessage(Txt.parse("<i>Opening warp GUI..."));
		
		openGUI(sender);
	}
	
	private void openGUI(CommandSender sender)
	{
		Inventory inv = Bukkit.createInventory(me, 36, Txt.parse("<aqua>Server Warps"));
		int i = 0;
		for (UWarp warp : UWarpColl.get().getAll())
		{
			if (inv.getItem(i)!=null || !inv.getItem(i).getType().equals(Material.AIR)) i=i+2;
			@SuppressWarnings("deprecation")
			Material m = Material.getMaterial(warp.getStack());
			ItemStack stack = new ItemStack(m, 1);
			ItemMeta meta = stack.getItemMeta();
			meta.setDisplayName(Txt.parse("<aqua>"+warp.getName()));
			meta.setLore(Arrays.asList(Txt.parse("<i><italic>"+warp.getDescription())));
			stack.setItemMeta(meta);
			inv.setItem(i, stack);
			i = i+2;
		}
		if (OptionalDependencies.get().isFactionsEnabled())
		{
			com.massivecraft.factions.entity.MPlayer msender = com.massivecraft.factions.entity.MPlayer.get(sender);
			final Faction fac = msender.getFaction();
		    ItemStack stack = new ItemStack(Material.BANNER, 1);
		    BannerMeta meta = (BannerMeta) stack.getItemMeta();
		    meta.setBaseColor(DyeColor.GREEN);
		    meta.setDisplayName(Txt.parse("<i>Faction: <lime><bold>"+fac.getName()));
		    meta.setLore(Arrays.asList(Txt.parse("<i><italic>"+fac.getDescription())));
		    stack.setItemMeta(meta);
		    inv.setItem(22, stack);
		}
		me.openInventory(inv);
	}
}
