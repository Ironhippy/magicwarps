package org.valdaynet.java.magwarps.cmd;

import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.VisibilityMode;

public class MagicCommand extends MassiveCommand {
	
	public MagicCommand(){
		this.setVisibilityMode(VisibilityMode.VISIBLE);
	}
}