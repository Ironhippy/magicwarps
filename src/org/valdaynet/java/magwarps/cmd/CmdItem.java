package org.valdaynet.java.magwarps.cmd;

import org.apache.commons.lang.StringUtils;
import org.bukkit.inventory.ItemStack;
import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.cmd.req.ReqHasItemInHand;
import org.valdaynet.java.magwarps.cmd.req.ReqHasWarpSelected;
import org.valdaynet.java.magwarps.entity.MPlayer;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.util.Txt;

public class CmdItem extends MagicCommand {
	
	public CmdItem()
	{
		this.addAliases("item");
		
		this.desc = "set default item for warp gui";
		
		this.addRequirements(ReqHasItemInHand.get(), ReqHasWarpSelected.get(), ReqHasPerm.get(Perm.CREATE.node));
	}
	
	@Override
	public void perform() throws MassiveException {
		ItemStack stack = me.getItemInHand();
		
		@SuppressWarnings("deprecation")
		int id = stack.getType().getId();
		String material = StringUtils.lowerCase(stack.getType().name());
		String Material = Txt.upperCaseFirst(material);
		
		String name = MPlayer.get(me).getSelected().getName();
		MPlayer.get(me).getSelected().setStack(id);
		
		sendMessage(Txt.parse("<i>You have now set <aqua>"+name+"<i>'s GUI item to <rose>"+Material+"<i>."));
	}
}
