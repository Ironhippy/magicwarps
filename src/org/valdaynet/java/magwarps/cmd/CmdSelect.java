package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.cmd.arg.ARWarp;
import org.valdaynet.java.magwarps.entity.MPlayer;
import org.valdaynet.java.magwarps.entity.UWarp;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.util.Txt;

public class CmdSelect extends MagicCommand {
	
	public CmdSelect()
	{
		this.addAliases("select", "s");
		
		this.desc = Txt.parse("<i>change warp settings");
		
		this.addRequirements(ReqIsPlayer.get(), ReqHasPerm.get(Perm.SELECT.node));
		
		this.addArg(ARWarp.get(), "warp");
	}
	@Override
	public void perform() throws MassiveException {
		UWarp warp = this.readArg();
		
		if (warp==null) { sendMessage(Txt.parse("<i>It seems the warp is null as of this moment.")); return; }
		if (warp.getName()==null) { sendMessage(Txt.parse("<i>It seems the warp name is null as of this moment.")); return; }
		
		String name = warp.getName();
		
		MPlayer player = MPlayer.get(sender);
		player.setSelected(warp);
		sendMessage(Txt.parse("<i>You have selected the <aqua>"+name+"<i>."));
	}
}
