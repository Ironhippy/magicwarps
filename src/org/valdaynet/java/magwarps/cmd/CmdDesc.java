package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.cmd.req.ReqHasWarpSelected;
import org.valdaynet.java.magwarps.entity.MPlayer;
import org.valdaynet.java.magwarps.entity.UWarp;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.arg.ARString;
import com.massivecraft.massivecore.util.Txt;

public class CmdDesc extends MagicCommand {
	
	public CmdDesc() 
	{
		this.addAliases("desc");
		
		this.desc = Txt.parse("set warp desc");
		
		this.addArg(ARString.get(), "desc", true);
		
		this.addRequirements(ReqHasWarpSelected.get());
	}
	
	@Override
	public void perform() throws MassiveException {
		String str = this.readArg();
		
		UWarp warp = MPlayer.get(sender).getSelected();
		
		warp.setDescription(Txt.parse("<silver><italic>"+str));
		sendMessage(Txt.parse("<i>You have set the warp description to: \""+str+"\"."));
	}
	
}
