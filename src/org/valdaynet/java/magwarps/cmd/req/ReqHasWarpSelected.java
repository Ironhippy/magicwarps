package org.valdaynet.java.magwarps.cmd.req;

import org.bukkit.command.CommandSender;
import org.valdaynet.java.magwarps.entity.MPlayer;

import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.req.ReqAbstract;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public class ReqHasWarpSelected extends ReqAbstract {
	
	private static final long serialVersionUID = 1L;
	
	private static ReqHasWarpSelected i = new ReqHasWarpSelected();
	public static ReqHasWarpSelected get() { return i; }

	@Override
	public boolean apply(CommandSender arg0, MassiveCommand arg1) {
		if (MUtil.isntPlayer(arg0)) return false;
		
		MPlayer mplayer = MPlayer.get(arg0);
		if (mplayer.getSelected()!=null) return true; 
		
		return false;
	}

	@Override
	public String createErrorMessage(CommandSender arg0, MassiveCommand arg1) {
		return Txt.parse("<red>You do not have a warp selected. <i>Please select one.");
	}
	
}