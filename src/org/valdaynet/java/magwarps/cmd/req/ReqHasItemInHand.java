package org.valdaynet.java.magwarps.cmd.req;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.req.ReqAbstract;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public class ReqHasItemInHand extends ReqAbstract {
	
	private static ReqHasItemInHand i = new ReqHasItemInHand();
	public static ReqHasItemInHand get() { return i; }

	private static final long serialVersionUID = 1L;

	@Override
	public boolean apply(CommandSender sender, MassiveCommand cmd) {
		if (MUtil.isntPlayer(sender)) return false;
		
		Player player = (Player) sender;
		if (player.getItemInHand().getType()!=Material.AIR)
		{
			return true;
		}
		return false;
	}

	@Override
	public String createErrorMessage(CommandSender arg0, MassiveCommand arg1) {
		return Txt.parse("<rose>Error parsing command. <i>You do not have an item in your hand.");
	}
}
