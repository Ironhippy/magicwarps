package org.valdaynet.java.magwarps.cmd.req;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.valdaynet.java.magwarps.event.HoverEvent;

import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.req.ReqAbstract;
import com.massivecraft.massivecore.mson.Mson;
import com.massivecraft.massivecore.util.Txt;

public class ReqFactionsEnabled extends ReqAbstract {
	
	private static ReqFactionsEnabled req = new ReqFactionsEnabled();
	public static ReqFactionsEnabled get() {return req;}
	
	private static final long serialVersionUID = 1L;

	@Override
	public boolean apply(CommandSender sender, MassiveCommand command) {
		if (Bukkit.getServer().getPluginManager().getPlugin("Factions")!=null)
		{
			if (Bukkit.getServer().getPluginManager().getPlugin("Factions").isEnabled())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public String createErrorMessage(CommandSender sender, MassiveCommand command) {
		HoverEvent event = new HoverEvent(Txt.parse("<i>Click here to download Factions."));
		Mson mson = Mson.parse("<i>If you are the server owner you may download Factions here.")
		.link("http://dev.bukkit.org/media/files/889/302/Factions.jar")
		.hoverEvent(event);
		mson.sendOne(sender);
		return Txt.parse("<rose>This command requires a Factions installation. This server does not appear to have Factions installed.");
	}
	
	
}
