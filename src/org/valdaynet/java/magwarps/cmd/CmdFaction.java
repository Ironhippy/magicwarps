package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.cmd.req.ReqFactionsEnabled;
import org.valdaynet.java.magwarps.entity.MConf;

import com.massivecraft.factions.Factions;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.mixin.TeleportMixinDefault;
import com.massivecraft.massivecore.mixin.TeleporterException;
import com.massivecraft.massivecore.teleport.Destination;
import com.massivecraft.massivecore.teleport.DestinationSimple;
import com.massivecraft.massivecore.util.Txt;

public class CmdFaction extends MagicCommand {
	
	public CmdFaction()
	{
		this.addAliases("faction", "f");
		
		this.desc = "teleport to faction home";
		
		this.addRequirements(ReqIsPlayer.get(), ReqHasPerm.get(Perm.TELEPORT.node), ReqFactionsEnabled.get());
	}
	
	@Override
	public void perform() throws MassiveException {
		
		MPlayer msender = MPlayer.get(sender);
		Faction msenderFaction = msender.getFaction();
		
		if (msenderFaction.getId()==Factions.ID_NONE)
		{
			sendMessage(Txt.parse("<rose>Wilderness does not have a supported faction home. <i>Please join a real faction."));
			return;
		}
		
		Destination destination = new DestinationSimple(msenderFaction.getHome(), msenderFaction.getName());
		
		if (destination.getDesc(sender)==null) {sendMessage(Txt.parse("<i>Msg is null")); return;}
		if (destination.getPs(sender)==null) {sendMessage(Txt.parse("<i>PS is null.")); return;}
		
		int time = MConf.get().getDelay();
		
		try {
			TeleportMixinDefault.get().teleport(me, destination, time);
		} catch (TeleporterException e) {
			e.printStackTrace();
		}
	}
}
