package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.cmd.arg.ARWarp;
import org.valdaynet.java.magwarps.entity.UWarp;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.util.Txt;

public class CmdMove extends MagicCommand {
	
	public CmdMove()
	{
		this.addAliases("move", "m");
		
		this.addRequirements(ReqHasPerm.get(Perm.MOVE.node), ReqIsPlayer.get());
		
		this.addArg(ARWarp.get(), "warp", true);
	}
	
	@Override
	public void perform() throws MassiveException {
		UWarp warp = this.readArg();
		
		warp.setTeleportPS(PS.valueOf(me));
		
		sendMessage(Txt.parse("<i>Set warp <aqua>"+warp.getName()+" <i>to <white>"+me.getWorld().getName()+" "+
		                      "<pink>"+me.getLocation().getBlockX()+
		                      "<i>, <pink>"+me.getLocation().getBlockY()+
		                      "<i>, <pink>"+me.getLocation().getBlockZ()));
	}
}
