package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.ListRegistry;
import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;

public class CmdList extends MagicCommand {
	
	public CmdList()
	{
		this.addAliases("list", "l");
		
		this.addRequirements(ReqHasPerm.get(Perm.TELEPORT.node));
	}
	
	@Override
	public void perform() throws MassiveException {
		ListRegistry.get().getStringWarpList(UWarpColl.get().getAll(), me);
	}
}
