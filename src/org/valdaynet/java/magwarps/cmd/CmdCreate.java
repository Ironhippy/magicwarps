package org.valdaynet.java.magwarps.cmd;

import org.bukkit.Location;
import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.arg.ARString;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.util.Txt;

public class CmdCreate extends MagicCommand {
	
	public CmdCreate()
	{
		this.addAliases("create", "cr");
		
		this.desc = Txt.parse("<i>create new warp.");
		
		this.addArg(ARString.get(), "name", true);
		
		this.addRequirements(ReqHasPerm.get(Perm.CREATE.node));
		this.addRequirements(ReqIsPlayer.get());
	}
	
	@Override
	public void perform() throws MassiveException {
		String str = this.readArg();
		
		UWarp warp = UWarpColl.get().create(str);
		Location location = new Location(me.getWorld(), me.getLocation().getX(), me.getLocation().getY()+2.0,
				                         me.getLocation().getZ(), me.getLocation().getYaw(), me.getLocation().getPitch());
		warp.teleportTo = PS.valueOf(location);
		warp.setName(str);
		warp.setDefault(false);
		warp.setDescription(Txt.parse("<i>Enjoy your stay."));
		
		sendMessage(Txt.parse("<lime>Successfully created warp <aqua>"+str));
	}
}
