package org.valdaynet.java.magwarps.cmd;

import java.util.List;

import org.valdaynet.java.magwarps.MagicWarps;
import org.valdaynet.java.magwarps.Perm;

import com.massivecraft.massivecore.cmd.HelpCommand;
import com.massivecraft.massivecore.cmd.VersionCommand;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.util.MUtil;

public class CmdWarp extends MagicCommand {
	
	private static CmdWarp warp = new CmdWarp();
	public static CmdWarp get() {return warp;}
	
	public CmdFaction cmdFaction = new CmdFaction();
	public CmdMove cmdMove = new CmdMove();
	public CmdRemove cmdRemove = new CmdRemove();
	public CmdList cmdList = new CmdList();
	public CmdGui cmdGui = new CmdGui();
	public CmdItem cmdItem = new CmdItem();
	public CmdCreate cmdCreate = new CmdCreate();
	public CmdSelect cmdSelect = new CmdSelect();
	public CmdDesc cmdDesc = new CmdDesc();
	public CmdTeleport cmdTeleport = new CmdTeleport();
	public VersionCommand versionCommand = new VersionCommand(MagicWarps.get(), Perm.VERSION.node, "v", "version");
	
	public CmdWarp()
	{
		this.addRequirements(ReqIsPlayer.get());
		
		this.addSubCommand(HelpCommand.get());
		this.addSubCommand(cmdTeleport);
		this.addSubCommand(cmdFaction);
		this.addSubCommand(cmdGui);
		this.addSubCommand(cmdList);
		this.addSubCommand(cmdCreate);
		this.addSubCommand(cmdMove);
		this.addSubCommand(cmdRemove);
		this.addSubCommand(cmdSelect);
		this.addSubCommand(cmdDesc);
		this.addSubCommand(cmdItem);
		this.addSubCommand(versionCommand);
	}
	
	@Override
	public List<String> getAliases()
	{
		return MUtil.list("warp", "wa");
	}
}
