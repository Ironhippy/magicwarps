package org.valdaynet.java.magwarps.cmd;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.valdaynet.java.magwarps.MagicWarps;
import org.valdaynet.java.magwarps.OptionalDependencies;
import org.valdaynet.java.magwarps.cmd.arg.ARWarp;
import org.valdaynet.java.magwarps.entity.MConf;
import org.valdaynet.java.magwarps.entity.UWarp;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.cmd.req.ReqIsPlayer;
import com.massivecraft.massivecore.mixin.TitleMixinDefault;
import com.massivecraft.massivecore.particleeffect.ParticleEffect;
import com.massivecraft.massivecore.util.Txt;

import net.milkbowl.vault.economy.Economy;

public class CmdTeleport extends MagicCommand {
	
	public CmdTeleport()
	{
		this.addAliases("teleport", "tp");
		
		this.addRequirements(ReqIsPlayer.get(), ReqHasPerm.get(org.valdaynet.java.magwarps.Perm.TELEPORT.node));
		
		this.addArg(ARWarp.get(), "warp", true);
	}
	
	@Override
	public void perform() throws MassiveException {
		final UWarp warp = this.readArg();
		
		if (warp == null) { sender.sendMessage(Txt.parse("<i>Warp is null.")); return; }
		if (warp.teleportTo==null) { sender.sendMessage(Txt.parse("<i>Location is null.")); return; }
		
		if (OptionalDependencies.get().isVaultEnabled())
		{
			Economy eco = MagicWarps.get().getEconomy();
			double b = MConf.get().getTeleportCharge();
			if (eco.getBalance(me)>b)
			{
				eco.withdrawPlayer(me, b);
			}
			else {
				sendMessage(Txt.parse("<rose>You have insufficient funds. <i>You cannot afford a warp at this time."));
				return;
			}
		}
		
		warp.teleport(((Player)sender));
		final int time = MConf.get().getDelay();
		
		if (MConf.get().canUseTitles())
		{
			Bukkit.getScheduler().scheduleSyncDelayedTask(MagicWarps.get(), new Runnable() {
				@Override
				public void run() {
					TitleMixinDefault.get().sendTitleMsg(me, 15, 50, 15, MConf.get().translateTeleportMsg(warp), Txt.parse(warp.getDescription()));
				    if (MConf.get().canUseParticles()) return;
				    ParticleEffect.FLAME.display(2, 2, 2, 1, 12, me.getLocation());
				    me.playSound(me.getLocation(), Sound.LEVEL_UP, 12, 12);
				}
			}, time);
		}
		else if (!MConf.get().canUseTitles())
		{
			Bukkit.getScheduler().scheduleSyncDelayedTask(MagicWarps.get(), new Runnable() {
				@Override
				public void run() {
					sendMessage(Txt.parse("<i>"+MConf.get().translateTeleportMsg(warp))+"\n"+Txt.parse(warp.getDescription()));
					if (MConf.get().canUseParticles()) return;
					ParticleEffect.FLAME.display(2, 2, 2, 1, 12, me.getLocation());
					me.playSound(me.getLocation(), Sound.LEVEL_UP, 12, 12);
				}
			}, time);
		}
	}
}
