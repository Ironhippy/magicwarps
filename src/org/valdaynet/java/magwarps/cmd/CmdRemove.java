package org.valdaynet.java.magwarps.cmd;

import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.cmd.arg.ARWarp;
import org.valdaynet.java.magwarps.entity.UWarp;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.util.Txt;

public class CmdRemove extends MagicCommand {
	
	public CmdRemove()
	{
		this.addAliases("remove", "r");
		
		this.addRequirements(ReqHasPerm.get(Perm.REMOVE.node));
		
		this.addArg(ARWarp.get(), "warp", true);
	}
	@Override
	public void perform() throws MassiveException {
		UWarp warp = this.readArg();
		String name = warp.getName();
		warp.detach();
		sendMessage(Txt.parse("<i>Deleted <aqua>"+name+"<i> from the warp list."));
	}
}
