package org.valdaynet.java.magwarps;

import org.bukkit.plugin.Plugin;
import org.valdaynet.java.magwarps.entity.MConf;

public class OptionalDependencies {
	
	private static OptionalDependencies c = new OptionalDependencies();
	public static OptionalDependencies get() {return c;}
	
	public void registerVault(Plugin plugin)
	{
		MConf.get().setEcon(true);
	}
	
	public void registerFactions(Plugin plugin)
	{
		MConf.get().setFac(true);
	}
	
	public void unregisterFactions()
	{
		MConf.get().setFac(false);
	}
	
	public void unregisterVault()
	{
		MConf.get().setEcon(false);
	}
	
	public boolean isFactionsEnabled()
	{
		return MConf.get().getFac();
	}
	public boolean isVaultEnabled()
	{
		return MConf.get().getEcon();
	}
}
