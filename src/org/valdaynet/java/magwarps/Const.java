package org.valdaynet.java.magwarps;

public class Const {
	
	public static final String BASENAME_ = "magicwarps_";
	
	public static final String COLLECTION_MPLAYER = BASENAME_+"mplayer";
	public static final String COLLECTION_WARPS = BASENAME_+"warps";
	public static final String COLLECTION_CONFIG = BASENAME_+"config";
}
