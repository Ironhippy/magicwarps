package org.valdaynet.java.magwarps.event;

import com.massivecraft.massivecore.mson.MsonEvent;
import com.massivecraft.massivecore.mson.MsonEventAction;

public class HoverEvent extends MsonEvent {
	
	private static final long serialVersionUID = 1L;

	public HoverEvent(String str) {
		super(MsonEventAction.SHOW_TEXT, str);
		super.hoverText(str);
	}
}
