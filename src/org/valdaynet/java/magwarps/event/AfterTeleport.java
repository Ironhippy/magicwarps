package org.valdaynet.java.magwarps.event;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.valdaynet.java.magwarps.MagicWarps;
import org.valdaynet.java.magwarps.entity.MConf;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.massivecore.event.EventMassiveCoreAfterPlayerTeleport;
import com.massivecraft.massivecore.mixin.TitleMixinDefault;
import com.massivecraft.massivecore.particleeffect.ParticleEffect;
import com.massivecraft.massivecore.util.Txt;

public class AfterTeleport implements Listener {
	
	private static AfterTeleport teleport = new AfterTeleport();
	public static AfterTeleport get() { return teleport; }
	
	public void listen()
	{
		Bukkit.getPluginManager().registerEvents(this, MagicWarps.get());
	}
	
	@EventHandler
	public void after(EventMassiveCoreAfterPlayerTeleport ev)
	{
		String warpName = null;
		for (UWarp warp : UWarpColl.get().getAll())
		{
			if (ev.getTo().getBlockX()==warp.getX() && ev.getTo().getBlockY()==warp.getY() && ev.getTo().getBlockZ()==warp.getZ())
			{
				if (warp.teleportTo.getWorld().equalsIgnoreCase(ev.getTo().getWorld().getName()))
				{
					warpName = warp.getName();
					break;
				}
			}
		}
		if (MConf.get().canUseTitles())
		{
			TitleMixinDefault.get().sendTitleMessage(ev.getPlayer(), 
					                                 15, 60, 15,
					                                 Txt.parse(MConf.get().translateTeleportMsg(UWarpColl.getByName(warpName))),
					                                 Txt.parse(Txt.parse(UWarpColl.getByName(warpName).getDescription())));
			if (!MConf.get().canUseParticles()) return;
			ParticleEffect.FLAME.display(2, 2, 2, 1, 12,ev.getPlayer().getLocation());
		    ev.getPlayer().playSound(ev.getPlayer().getLocation(), Sound.LEVEL_UP, 12, 12);
		} 
		else if (!MConf.get().canUseTitles()) 
		{
			if (!MConf.get().canUseParticles()) return;
			ParticleEffect.FLAME.display(2, 2, 2, 1, 12,ev.getPlayer().getLocation());
		    ev.getPlayer().playSound(ev.getPlayer().getLocation(), Sound.LEVEL_UP, 12, 12);
		}
	}

}
