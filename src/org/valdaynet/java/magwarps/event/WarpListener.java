package org.valdaynet.java.magwarps.event;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.valdaynet.java.magwarps.Perm;
import org.valdaynet.java.magwarps.entity.MConf;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.entity.UWarpColl;

import com.massivecraft.massivecore.util.Txt;

public class WarpListener implements Listener {
	
	private static WarpListener warpListener = new WarpListener();
	public static WarpListener get() { return warpListener; }
	
	public void listen(Plugin plugin)
	{
		plugin.getServer().getPluginManager().registerEvents(WarpListener.get(), plugin);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent ev)
	{
		if (ev.getInventory().getName().equalsIgnoreCase(Txt.parse("<aqua>Server Warps")))
		{
			ItemStack clicked = ev.getCurrentItem();
			if (!clicked.getItemMeta().getDisplayName().contains("Faction: "))
			{
			    String ds = ChatColor.stripColor(clicked.getItemMeta().getDisplayName());
			
			    Bukkit.dispatchCommand(ev.getWhoClicked(), "warp tp "+ds);
			}
			else {
				Bukkit.dispatchCommand(ev.getWhoClicked(), "warp faction");
			}
			ev.getWhoClicked().getOpenInventory().close();
		}
	}
	@EventHandler
	public void onDeath(EntityDeathEvent ev)
	{
		if (ev.getEntityType()==EntityType.PLAYER)
		{
			Player player = (Player) ev.getEntity();
			if (player.hasPermission(Perm.SPAWN.node))
			{
				String str = MConf.get().defaultWarp();
				UWarp warp = UWarpColl.getByName(str);
				Location loc = warp.teleportTo.asBukkitLocation();
				player.setHealth(player.getMaxHealth());
				player.teleport(loc);
			}
		}
	}
	@EventHandler
	public void onSign(SignChangeEvent ev)
	{
		if (ev.getLine(0).equalsIgnoreCase("[Warp]"))
		{
			if (!ev.getPlayer().hasPermission(Perm.CREATE.node)) { ev.getPlayer().sendMessage(Txt.parse("<rose>Insufficient Permissions. <i>You may not make a sign warp.")); return; }
			if (ev.getLine(1)!=null)
			{
				String str = ev.getLine(1);
				if (UWarpColl.getByName(str)!=null)
				{
					ev.setLine(0, Txt.parse("<i>[<aqua>Warp<i>]"));
					ev.setLine(1, Txt.parse("<lime>"+str));
					ev.getPlayer().sendMessage(Txt.parse("<i>You have created a sign warp. <lime>Right click it to teleport."));
					ev.getPlayer().playSound(ev.getBlock().getLocation(), Sound.LEVEL_UP, 10, 20);
				} else {
					ev.getPlayer().sendMessage(Txt.parse("<rose>The warp you specified does not exist. <i>Please try again."));
				}
			}
		}
	}
	@EventHandler
	public void onSignWarp(PlayerInteractEvent ev)
	{
		if (ev.getAction()==Action.RIGHT_CLICK_BLOCK)
		{
			if (ev.getClickedBlock().getType()==Material.WALL_SIGN || ev.getClickedBlock().getType()==Material.SIGN_POST)
			{
				Sign s = (Sign) ev.getClickedBlock().getState();
				String check = s.getLine(0);
				if (check.equalsIgnoreCase(Txt.parse("<i>[<aqua>Warp<i>]")))
				{
					if (s.getLine(1)!=null)
					{
						String line = ChatColor.stripColor(s.getLine(1));
						if (UWarpColl.getByName(line)!=null)
						{
							if (!ev.getPlayer().hasPermission(Perm.TELEPORT.node)) return;
							Bukkit.dispatchCommand(ev.getPlayer(), "warp tp "+line);
						}
						else {
							ev.getPlayer().sendMessage(Txt.parse("<rose>This warp does not exist. <i>Please contact an administrator if you believe this to be an error."));
						}
					} else {
						return;
					}
				}
			}
		}
	}
}