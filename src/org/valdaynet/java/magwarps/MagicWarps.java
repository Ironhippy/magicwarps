package org.valdaynet.java.magwarps;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.valdaynet.java.magwarps.cmd.CmdWarp;
import org.valdaynet.java.magwarps.entity.MConfColl;
import org.valdaynet.java.magwarps.entity.MPlayerColl;
import org.valdaynet.java.magwarps.entity.UWarpColl;
import org.valdaynet.java.magwarps.event.AfterTeleport;
import org.valdaynet.java.magwarps.event.WarpListener;

import com.massivecraft.massivecore.MassivePlugin;

import net.milkbowl.vault.economy.Economy;

public class MagicWarps extends MassivePlugin {
	
	private static MagicWarps r;
	public MagicWarps() { MagicWarps.r = this; }
	public static MagicWarps get() { return r; }
	
	public boolean isDatabaseInitialized = false;
	
	public static final String USPAWN_ID = "Uspawn";
	public static final String CONFIG_ID = "default_config";
	
	public CmdWarp cmdWarp = CmdWarp.get();
	
	@SuppressWarnings("unused")
	private static Economy econ = null;
	
	@Override
	public void onEnable()
	{
		if (!preEnable()) return;
		
		isDatabaseInitialized = false;
		
		UWarpColl.get().init();
		MPlayerColl.get().init();
		
		defaultStart();
		
		MConfColl.get().init();
		
		runVaultCheck();
		runFactionsCheck();
		
		cmdWarp.register(this);
		
		getServer().getPluginManager().registerEvents(new WarpListener(), this);
		
		
		getServer().getPluginManager().registerEvents(new AfterTeleport(), this);
		
		isDatabaseInitialized = true;
		
		postEnable();
	}
	@Override
	public void onDisable()
	{
		OptionalDependencies depend = OptionalDependencies.get();
		depend.unregisterFactions();
		depend.unregisterVault();
		super.onDisable();
	}
	private void defaultStart()
	{
		if (UWarpColl.get().get(USPAWN_ID)!=null) return;
		
		UWarpColl.get().loadDefault();
	}
	public Economy getEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault")==null) return null;
		RegisteredServiceProvider<Economy> economy = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economy == null || economy.getProvider() == null) {
        	this.getLogger().severe("There is no economy provider to support Vault, make sure you installed an economy plugin");
        	return null;
        }		
	    return economy.getProvider();
	}
	private boolean runVaultCheck()
	{
		if (getServer().getPluginManager().getPlugin("Vault")==null) { return false; } 
		if (getEconomy()==null) {
			return false;
		}
		else if (getEconomy()!=null) {
			econ = getEconomy();
		}
		OptionalDependencies depend = OptionalDependencies.get();
		depend.registerVault(this);
		return true;
	}
	private boolean runFactionsCheck()
	{
		if (getServer().getPluginManager().getPlugin("Factions")==null) return false;
		OptionalDependencies depend = OptionalDependencies.get();
		depend.registerFactions(this);
		return true;
	}
}