package org.valdaynet.java.magwarps;

import java.util.Collection;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.valdaynet.java.magwarps.entity.UWarp;
import org.valdaynet.java.magwarps.event.HoverEvent;

import com.massivecraft.massivecore.mson.Mson;
import com.massivecraft.massivecore.util.Txt;

public class ListRegistry {
	
	private static ListRegistry listRegis = new ListRegistry();
	public static ListRegistry get() { return listRegis; }
	
	public void getStringWarpList(Collection<UWarp> warps, Player player)
	{
		String line = Txt.parse("<orange>_________.[ <lime>Warp List <orange>]._________\n");
		player.sendMessage(line);
		for (UWarp warp : warps)
		{
			HoverEvent ev = new HoverEvent(Txt.parse("<i>Click to teleport to <aqua>"+warp.getName()));
			
			String msonTxt = Txt.parse("<i>- {<pink>"+warp.getName()+"<i>} <silver>== <i>"+
		                             "[<pink>"+warp.teleportTo.getWorld()+"<i>, "+
					                 "<pink>"+warp.getX()+"<i>, "+
		                             "<pink>"+warp.getY()+"<i>, "+
					                 "<pink>"+warp.getZ()+"<i>]");
			player.sendMessage(msonTxt);
			Mson mson = Mson.parse("<rose>Click here to teleport to <aqua>"+warp.getName()+"\n").color(ChatColor.AQUA).command("/warp tp "+warp.getName()).hoverEvent(ev);
			mson.sendOne(player);
		}
		return;
	}
}
