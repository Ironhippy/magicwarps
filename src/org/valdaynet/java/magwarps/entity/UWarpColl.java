package org.valdaynet.java.magwarps.entity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.valdaynet.java.magwarps.Const;
import org.valdaynet.java.magwarps.MagicWarps;

import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import com.massivecraft.massivecore.util.Txt;

public class UWarpColl extends Coll<UWarp> {
	
	private static UWarpColl i = new UWarpColl(Const.COLLECTION_WARPS);
	public static UWarpColl get() {return i;}
	
	private UWarpColl(String name) {
		super(name, UWarp.class, MStore.getDb(), MagicWarps.get());
	}
	
	@Override
	public void init()
	{
		super.init();
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
	}
	
	@Override
	public UWarp get(Object id)
	{
		if (super.get(id)==null) return null;
		
		return super.get(id);
	}
	
	public static UWarp getByName(String str)
	{
		for (UWarp warp : UWarpColl.get().getAll())
		{
			if (warp.getName().equalsIgnoreCase(str))
			{
				Bukkit.getConsoleSender().sendMessage(Txt.parse("<green>[<i>MagicWarps<green>] <i>Passed."));
				return warp;
			}
		}
		return null;
	}
	
	public void loadDefault()
	{
		String oid = MagicWarps.USPAWN_ID;
		UWarp warp = this.create(oid);
		warp.setDefault(true);
		warp.setName("Uspawn");
		World w = Bukkit.getWorlds().get(0);
		Location loc = new Location(w, w.getSpawnLocation().getX(), w.getSpawnLocation().getY()+2, w.getSpawnLocation().getZ());
		PS ps = PS.valueOf(loc);
		warp.setTeleportPS(ps);
		warp.setDescription(Txt.parse("<i>The Universal Spawn Location"));
	}
}
