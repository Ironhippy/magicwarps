package org.valdaynet.java.magwarps.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.Txt;

public class MConf extends Entity<MConf> {
	
	protected static transient MConf i;
	public static MConf get() { return i; }
	
	private boolean factionsEnabled = false;
	
	private boolean econEnabled = false;
	
    private String defaultWarp = "Uspawn";
    
    private String warpTeleportMessage = "<i>Welcome to <aqua>";
    
    private Integer delayTime = -1;
	
	private Boolean canUseParticles = true;
	
	private Boolean canUseTitles = false;
	
	private double econChargeGUI = 0;
	
	private double econChargeTeleport = 0;
	
	@Override
	public MConf load(MConf that)
	{
		super.load(that);
		
		return this;
	}
	
	public String defaultWarp() { return this.defaultWarp; }
	
	public boolean canUseParticles() { return this.canUseParticles; }
	
	public boolean canUseTitles() { return this.canUseTitles; }
	
	public void setTitles(boolean bool)
	{
		this.canUseTitles = bool;
		this.changed();
	}
	public void setParticlesEnabled(boolean bool)
	{
		this.canUseParticles = bool;
		this.changed();
	}
	public void setDefaultWarp(UWarp warp)
	{
		String id = warp.getId();
		this.defaultWarp = id;
		this.changed();
	}
	public String translateTeleportMsg(UWarp warp)
	{
		String str = Txt.parse(this.warpTeleportMessage+warp.getName()+"<i>.");
		return str;
	}
	public void setWarpMessage(String parse)
	{
		this.warpTeleportMessage = Txt.parse(parse);
		this.changed();
	}
	public int getDelay()
	{
		return this.delayTime;
	}
	public boolean getEcon()
	{
		return this.econEnabled;
	}
	public void setEcon(boolean bool) {
		this.econEnabled = bool;
		this.changed();
	}
	public boolean getFac()
	{
		return this.factionsEnabled;
	}
	public void setFac(boolean bool) {
		this.factionsEnabled = bool;
		this.changed();
	}
	public double getBookCharge()
	{
		return this.econChargeGUI;
	}
	public double getTeleportCharge()
	{
		return this.econChargeTeleport;
	}
}
