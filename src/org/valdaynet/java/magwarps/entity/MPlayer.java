package org.valdaynet.java.magwarps.entity;

import com.massivecraft.massivecore.store.SenderEntity;

public class MPlayer extends SenderEntity<MPlayer> {
	
	private UWarp selected = null;
	private boolean canUseWarp = true;
	
	public static MPlayer get(Object uid)
	{
		return MPlayerColl.get().get(uid);
	}
	
	public void setSelected(UWarp warp)
	{
		this.selected = warp;
	}
	public UWarp getSelected()
	{
		return this.selected;
	}
	public void muteWarping(boolean bool)
	{
		this.canUseWarp = bool;
	}
	public boolean canWarp()
	{
		return this.canUseWarp;
	}
}
