package org.valdaynet.java.magwarps.entity;

import org.valdaynet.java.magwarps.Const;
import org.valdaynet.java.magwarps.MagicWarps;

import com.massivecraft.massivecore.store.MStore;
import com.massivecraft.massivecore.store.SenderColl;

public class MPlayerColl extends SenderColl<MPlayer> {
	
	private static MPlayerColl mpl = new MPlayerColl();
	public static MPlayerColl get() { return mpl; }

	public MPlayerColl() {
		super(Const.COLLECTION_MPLAYER, MPlayer.class, MStore.getDb(), MagicWarps.get());
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
	}
	@Override
	public void init()
	{
		super.init();
	}
}
