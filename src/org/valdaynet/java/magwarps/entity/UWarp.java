package org.valdaynet.java.magwarps.entity;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.massivecraft.massivecore.mixin.TeleportMixinDefault;
import com.massivecraft.massivecore.mixin.TeleporterException;
import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.teleport.Destination;
import com.massivecraft.massivecore.teleport.DestinationSimple;
import com.massivecraft.massivecore.util.Txt;

public class UWarp extends Entity<UWarp> {
	
	private String desc = Txt.parse("<i>Example Description");
	private String name = Txt.parse("Example Name");
	private boolean isDefault = true;
	private int guiItem = 1;
	public PS teleportTo = PS.valueOf(Bukkit.getWorlds().get(0).getSpawnLocation());
	
	public PS setTeleportPS(PS ps)
	{
		teleportTo = ps;
		return teleportTo;
	}
	
	public boolean setDefault(boolean that)
	{
		isDefault = that;
		return isDefault;
	}
	
	public boolean getDefault()
	{
		return isDefault;
	}
	
	public String setName(String id)
	{
		this.name = id;
		return this.name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void teleport(Player player)
	{
			Destination dest = new DestinationSimple(this.teleportTo);
			try {
				TeleportMixinDefault.get().teleport(player, dest, MConf.get().getDelay());
			} catch (TeleporterException e) {
				e.printStackTrace();
			}
	}
	
	public String getDescription()
	{
		return this.desc;
	}
	
	public String setDescription(String desc)
	{
		this.desc = desc;
		return this.desc;
	}
	public int setStack(int stack)
	{
		this.guiItem = stack;
		return this.guiItem;
	}
	public int getStack()
	{
		return this.guiItem;
	}
	public int getX()
	{
		return PS.asBukkitLocation(teleportTo).getBlockX();
	}
	public int getY()
	{
		return PS.asBukkitLocation(teleportTo).getBlockY();
	}
	public int getZ()
	{
		return PS.asBukkitLocation(teleportTo).getBlockZ();
	}
}