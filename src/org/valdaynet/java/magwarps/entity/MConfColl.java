package org.valdaynet.java.magwarps.entity;

import org.valdaynet.java.magwarps.Const;
import org.valdaynet.java.magwarps.MagicWarps;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;

public class MConfColl extends Coll<MConf> {
	
	private static MConfColl i = new MConfColl();
	public static MConfColl get() { return i; }

	private MConfColl() {
		super(Const.COLLECTION_CONFIG, MConf.class, MStore.getDb(), MagicWarps.get());
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
	}
	@Override
	public void init()
	{
		super.init();
		
		MConf.i = this.get(MassiveCore.INSTANCE, true);
		
		loadDefault();
	}
	public void loadDefault()
	{
		String id = MConf.i.defaultWarp();
		if (MConf.i.defaultWarp()=="Uspawn") { 
			if (UWarpColl.get().get(MagicWarps.USPAWN_ID).getName()!="Uspawn")
			{
				UWarpColl.get().get(MagicWarps.USPAWN_ID).setName(id);
			}
		}
		else { 
			UWarpColl.get().get(MagicWarps.USPAWN_ID).setName(id);
		}
	}
}
