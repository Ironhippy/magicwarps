package org.valdaynet.java.magwarps;

import org.valdaynet.java.magwarps.entity.UWarp;

public enum Perm {
	
	SPAWN("spawn"),
	TELEPORT("teleport"),
	SELECT("mod.select"),
	CREATE("mod.create"),
	REMOVE("mod.remove"),
	NAME("mod.name"),
	DESC("mod.desc"),
	MOVE("mod.move"),
	VERSION("version");
	
	public String node = null;
	
	private Perm(String node){
		this.node = "magicwarps."+node;
	}
	
	@Deprecated
	public String getWarpPerm(UWarp warp)
	{
		String id = null;
		if (warp.isDefault())
		{
			return TELEPORT.node;
		}
		else if (!warp.isDefault())
		{
			return TELEPORT.node+warp.getName();
		}
		
		return id;
	}
}
